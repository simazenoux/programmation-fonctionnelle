
1.(quad x) retourne la puissance quatrième de x
(define quad 
    (lambda (x)
        (* x x x x)
    )
)

2.(cercle r) retourne la liste contenant la circonférence etla surface d'un cercle de rayon r
(define cercle 
    (lambda (r)
        (let ((pi 3.14159))
            (list (* 2 pi r) (* pi r r)) 
        ) 
    )
)

3. (fac n) retourne la factorielle de n
(define fac 
    (lambda (n)
        (if (= n 0) 
            1 
            (* n (fac (-n 1)))
        ) 
    )
)

4. (som_int n) retourne la somme des n premiers entiers non nuls
(define som_int 
    (lambda (n)
        (if (= n 1) 
            1 
            (+ n (som_int (-n 1)))
        )
    )
)

5. (long L) retourne la longueur de la liste L
(define long 
    (lambda (L)
        (if (null? L) 
            0 
            (+ 1 (long (cdr L)))
        )
    )
)

6. (miroir L) retourne la liste L renversée
(define miroir 
    (lambda (L) 
        (m L())
    )
)
(define m 
    (lambda (L LM)
        (if (null? L)
            LM
            (m (cdr L) (cons (car L) LM))
        )
    )
)

7. (carre L) retourne la liste des carrés des nombres contenus dans L
(define carre 
    (lambda (L)
        (if (null? L) 
            ()
            (if (number? (car L)) 
                (cons (* (car L) (car L)) (carre (cdr L)))
                (carre (cdr L)) 
            )
        )
    )
)

8. (nbpos L) retourne le nombre de nombres positifs contenus dans L
(define nbpos 
    (lambda (L)
        (if (null? L)
            0
            (if (and (number? (car L)) (> (car L) 0)) 
                (+ 1 (nbpos (cdr L)))
                (nbpos (cdr L)) 
            )
        )
    )
)

9. (membre x L) retourne #t si x est membre de L, et #f sinon
(define membre (lambda (x L)(if (null? L) #f(or (equal? x (car L)) (membre x (cdr L)))) ))

10. (epure L) retourne la liste L sans double
(define epure (lambda (L)(if (null? L) ()(let ((LE (epure (cdr L))))(if (membre (car L) LE) LE (cons (car L) LE)) )) ))

11. (nieme n L) retourne le nième élément de la liste L
(define nieme (lambda (n L)(if (null? L) "n est superieura la longueur de L"(if (= n 1) (car L) (nieme (-n 1) (cdr L)))) ))


12. (insere n x L) retourne la liste L où, si  cela est possible,x est inséré en nième position, et sinon retourne L
(define insere 
    (lambda (n x L)
        (if (null? L) 
            (if (=n 1) 
                (list x) 
                ()
            )
            (if (= n 1) 
                (cons x L)
                (cons (car L) (insere (-n 1) x (cdr L)))
            )
        )
    )
)

13. (union L1 L2) retourne l'union des ensembles L1 et L2
(define union
    (lambda (L1 L2)
        (if (null? L1)
            L2
            (if (membre (car L1) L2)
                (union L2 (cdr L1))
                (cons (car L1) (union L2 (cdr L1))) 
            )
        ) 
    )
)
ou bien :(define union(lambda (L1 L2)(epure (append L1 L2)) ) )

14.(inter L1 L2) retourne l'intersection des ensembles L1 et L2
(define inter
    (lambda (L1 L2)
        (if (null? L1)
            ()
            (if (membre (car L1) L2)
                (cons (car L1) (inter L2 (cdr L1)))
                (inter L2 (cdr L1)) 
            )
        )
    )
)


15. (niv0 L) retourne la liste L "mise à plat"
(define niv0 (lambda (L)(if (null? L)()(if (list? (car L))(append (niv0 (car L))(niv0 (cdr L)))(cons (car L) (niv0 (cdr L))) )) ))

16. (zip L1 L2) retourne la liste des couples des éléments de L1 et L2qui sont en positions identiques
(define zip(lambda (L1 L2)(if (or (null? L1)(null? L2)) ()(cons (list (car L1) (car L2)) (zip (cdr L1) (cdr L2)))) ))
ou bien :(define zip(lambda (L1 L2)(map list L1 L2) ) )

17. (prod L1 L2) retourne la liste des couples du produit cartésien de L1 et L2
(define prod(lambda (L1 L2)(if (null? L)()(append(coupler (car L1) L2) (prod (cdr L1) L2))) ) )
(define coupler(lambda (x L)(if (null? L)()(cons (list x (car L)) (coupler x (cdr L)))) ) )
ou bien :(define prod(lambda (L1 L2)(map (lambda (x1) (map (lambda (x2) (list x1 x2)) L2)) L1) ) )

18. (som_list L) retourne la somme des éléments numériques d'une liste,s'il y en a, et un message d'erreur sinon
Schéma récursif :
(define som_list (lambda (L)(if (null? L) "il n'y a pas d'element numerique dans la liste"(let ((Rcdr (som_list (cdr L))))(if (number? (car L))(+ (car L) (if (number? Rcdr) Rcdr 0))Rcdr ) )) ))

Schéma itératif :(define som_list (lambda (L)(let ((message "il n'y a pas d'element numerique dans la liste"))(SL L message) ) ))
(define SL (lambda (L R)(if (null? L)R(if (number? (car L))(SL (cdr L) (+ (car L) (if (number? R) R 0)))(SL (cdr L) R) )) ))
Ou bien :(define som_list (lambda (L)(let ((LN (filtrer number? L)))(if (null? LN)"il n'y a pas d'element numerique dans la liste"(apply + LN)) ) ))

19. (triang n) retourne la liste (1 2 ... n-1 n n-1 ... 2 1)
Schéma itératif:(define triang (lambda (n) (T1 n '(1))))(define T1 (lambda (n LT)(if (< (car LT) n)(T1 n (cons (+ (car LT) 1) LT))(T2 LT) ) ))
(define T2 (lambda (LT)(if (> (car LT) 1)(T2 (cons (-(car LT) 1) LT))LT ) ))

20. (fibo n) retourne le nième terme de la suite de Fibonaccidéfinie par :u0=u1=1 et un=(un-1)+(un-2) pour n>=2
(define fibo (lambda (n)(if (= n 0) 1 (car (fibo2 n))) ))(define fibo2 (lambda (n)(if (= n 1)'(1 1)(let* ((C (fibo2 (-n 1)))(un-1 (car C))(un-2 (cadr C)) )(list (+ un-1 un-2) un-1) ) ) ))

21. (moy L) retourne la moyenne des éléments numériques de Len n'effectuant qu'un seul parcours de LSchéma récursif (SL retourne le couple effectif-somme):
(define moy (lambda (L)(let ((C (SL L)))(if (> (car C) 0)(/ (cadr C) (car C))"il n'y a pas d'element numerique dans la liste" ) ) ) )
(define SL(lambda (L)(if (null? L)'(0 0)(let ((C (SL (cdr L))))(if (number? (car L))(list (+ 1 (car C)) (+ (car L) (cadr C)))C ) )) ))
Schéma itératif :(define moy (lambda (L) (m L 0 0)))(define m (lambda (L e s)(if (null? L)(if (> e 0)(/ s e)"il n'y a pas d'element numerique dans la liste" )(if (number? (car L))(m (cdr L) (+ 1 e) (+ (car L) s))(m (cdr L) e s) )) ))

22. (ies L n) retourne une liste contenant la liste des nombres inférieurs à n,la liste des nombres égaux à n et la liste des nombres supérieurs à n
(define ies(lambda (L n)(if (null? L) '(() () ())(let ((T (ies (cdr L) n)))(cond((< (car L) n) (list (cons (car L) (car T)) (cadr T) (caddr T)))((= (car L) n) (list (car T) (cons (car L) (cadr T)) (caddr T)))((> (car L) n) (list (car T) (cadr T) (cons (car L) (caddr T))))) )) ))


23. (tri_ins L) retourne la liste L triée par insertion (ordre croissant)
(define tri_ins(lambda (L)(if (null? L) ()(insertion (car L) (tri_ins (cdr L)))) ))(define insertion(lambda (x L)(if (null? L)(list x)(if (> x (car L))(cons (car L) (insertion x (cdr L)))(cons x L) )) ))

24.(tri_sel L) retourne la liste L triée par sélection (ordre croissant)
(define tri_sel (lambda (L)(if (null? L)()(let* ((C (min-lsmin L)) (min (car C)) (lsmin (cadr C)))(cons min (tri_sellsmin)) )) ))(min-lsmin L) retourne le couple contenant un élément minimum de L non videet la liste L tronquée de cet élément (solution itérative)(define min-lsmin (lambda (L) (mlsm (cdr L) (car L) ())))(define mlsm (lambda (L m lsm)(if (null? L)(list m lsm)(if (< (car L) m)(mlsm (cdr L) (car L) (cons m lsm))(mlsm (cdr L) m (cons (car L) lsm)) )) ))

25.(tri_bul L) retourne la liste L triée par bulle (ordre croissant)
(define tri_bul(lambda (L)(if (null?L)()(let ((BL (bulle L)))(cons (car BL) (tri_bul (cdr BL))) )) ))

(define bulle(lambda (L)(if (null?(cdr L)) L(let ((Bcdr (bulle (cdr L))))(if (> (car L) (car Bcdr))(cons (car Bcdr) (cons (car L) (cdr Bcdr)))(cons (car L) Bcdr) ) )) ))26.(tri_fus L) retourne la liste L triée par fusion (ordre croissant)(define tri_fus(lambda (L)(if (or (null? L)(null? (cdr L))) L(let ((LD (diviser L)))(fusion (tri_fus (car LD)) (tri_fus (cadr LD))) )) ))(define diviser(lambda (L)(if (or (null? L)(null? (cdr L))) (list L ())(let ((Dcddr (diviser (cddr L))))(list(cons (car L) (car Dcddr))(cons (cadr L) (cadr Dcddr)) ) )) ))(define fusion(lambda (L1 L2)(if (or (null? L1)(null? L2)) (append L1 L2)(if (> (car L1) (car L2))(cons (car L2) (fusion L1 (cdr L2)))(cons (car L1) (fusion (cdr L1) L2)) )) ))27. (tri_rap L) retourne la liste L triée par tri rapide (ordre croissant)(define tri_rap(lambda (L)(if (or (null? L)(null? (cdr L))) L(let ((LD (diviser L)))(append (tri_rap (car LD)) (tri_rap (cadr LD))) )) ))(define diviser (lambda (L) (d (cdr L) (car L) (list (car L)) ())))(define d (lambda (L p L1 L2)(if (null? L)(list L1 L2)(if (> (car L) p)(d (cdr L) p L1 (cons (car L) L2))(d (cdr L) p (cons (car L) L1) L2) )) ))
